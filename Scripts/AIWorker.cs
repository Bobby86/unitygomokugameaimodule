﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;


public class AIWorker 
{
	#region Public References
	//	public static AIController Instance;
//	public bool isAITurn = false;
	public int aiReturnXCoord;
	public int aiReturnYCoord;
	#endregion


	#region Private References
	private bool restrictedField = false;
	private int mColumns;
	private int nStrings;
	//	private int[] aiReturnCellCoord;

	private System.Random random;
	private int[] tokens; // 'matrix of tokens': 0 - free cell, 1 - player token, 2 - AI token
	private int[] patternWeights;
	//	private float[] cellWeights;	// weights of empty cells on the field. '-1' if it's not empty
	private Regex[] patternAttack;
	private Regex[] patternDefence;
	private List <List<int>> priorityCellIds = new List<List<int>> ();
	private List <CellStructure> emptyCells = new List<CellStructure> ();
	private List <CellStructure> playerCells = new List<CellStructure> ();
	private List <CellStructure> aiCells = new List<CellStructure> ();
//	private List <List <CellStructure>> priorityCellsByPatterns = new List<List<CellStructure>> ();
	#endregion


	//	#region Unity Callbacks
	//	void Awake ()
	//	{
	//		Instance = this;
	//	}
	//
	//
	//	void Start ()
	//	{
	//		patternWeights = new int[PrePatternsModel.PrePatterns.Count];
	//		patternAttack = new Regex[patternWeights.Length];
	//		patternDefence = new Regex[patternWeights.Length];
	//
	////		aiReturnCellCoord = new int[2];
	//		// Debugs // //
	////		Initialize (8, 16);
	//		// // // // //
	//	}
	//	#endregion


	#region Public Methods
	/// <summary>
	/// Initialize the specified numberOfColumns and numberOfStrings.
	/// </summary>
	/// <param name="numberOfColumns"> Number of columns. </param>
	/// <param name="numberOfStrings"> Number of strings. </param>
	public AIWorker (int numberOfColumns, int numberOfStrings)
	{
		random = new System.Random (System.DateTime.Now.Millisecond);

		mColumns = numberOfColumns;
		nStrings = numberOfStrings;
		//		int fieldSize = mColumns * nStrings;
		tokens = new int[mColumns * nStrings];
		//		cellWeights = new float[fieldSize];
		patternWeights = new int[PrePatternsModel.PrePatterns.Count];
		patternAttack = new Regex[patternWeights.Length];
		patternDefence = new Regex[patternWeights.Length];

//		priorityCellsByPatterns.Add (new List<CellStructure> ());	// pattern[0] priority  	// open 4
//		//		priorityCellsByPatterns.Add (new List<CellStructure> ());	// pattern[0] priority (for defence)
//		priorityCellsByPatterns.Add (new List<CellStructure> ());	// pattern[1] priority (for attack)		// 2 sides open 3 
//		priorityCellsByPatterns.Add (new List<CellStructure> ());	// pattern[1] priority (for defence)	// 2 sides open 3
//		priorityCellsByPatterns.Add (new List<CellStructure> ());	// pattern[2] priority (for attack)		// 1 side open 3
//		priorityCellsByPatterns.Add (new List<CellStructure> ());	// pattern[3] priority (for attack)		// 1 side open 3
//		priorityCellsByPatterns.Add (new List<CellStructure> ());	// pattern[2] priority (for defence)	// 1 side open 3
//		priorityCellsByPatterns.Add (new List<CellStructure> ());	// pattern[3] priority (for defence)	// 1 side open 3
		//	~same~	//
		priorityCellIds.Add (new List<int> ());	// pattern[0] priority  	// open 4
		priorityCellIds.Add (new List<int> ());	// pattern[1] priority (for attack)		// 2 sides open 3 
		priorityCellIds.Add (new List<int> ());	// pattern[1] priority (for defence)	// 2 sides open 3
		priorityCellIds.Add (new List<int> ());	// pattern[2] priority (for attack)		// 1 side open 3
		priorityCellIds.Add (new List<int> ());	// pattern[3] priority (for attack)		// 1 side open 3
		priorityCellIds.Add (new List<int> ());	// pattern[2] priority (for defence)	// 1 side open 3
		priorityCellIds.Add (new List<int> ());	// pattern[3] priority (for defence)	// 1 side open 3

		Initialize ();
	}

	/// <summary>
	/// Pass null argument if AI turn is first in the game.
	/// Return [x, y] coordinates of the cell for AI turn. (Error If x or(and) y equal -1)
	/// </summary>
	/// <param name="args"> . </param>
	public void AITurn (object args)//int playerXCellCoord = -1, int playerYCellCoord = -1)
	{		
//		isAITurn = true;
		CleanUp ();

		if (args == null)	// x coord comparition
		{			
			AIFirstTurn (-1, -1, true);
		}
		else
		{
			int[] playerCoords = (int[]) args;

			if (playerCells.Count > 0)
			{
				PutAToken (playerCoords[0], playerCoords[1], playerCells, true, !restrictedField); // passing x, y of new player's cell coordinates
				// recalculate weights of the cells
				CalculateAITurn (restrictedField);	// pass 'true' argument for restricted field
			}
			else
			{
				AIFirstTurn (playerCoords[0], playerCoords[1]);
			}
		}

		if (aiReturnXCoord > -1)
		{
			PutAToken (aiReturnXCoord, aiReturnYCoord, aiCells, false);
			//				Debug.Log ("[" + playerXCellCoord + "][" + playerYCellCoord + "]");
			//				Debug.Log ("[" + aiReturnXCoord + "][" + aiReturnYCoord + "]");
		}
		else
		{
							Debug.Log ("BUG [x, y] == -1");
		}
//		isAITurn = false; // TODO
//		return new int[2] {aiReturnXCoord, aiReturnYCoord};
		//		return new int[] {aiReturnCellCoord[0], aiReturnCellCoord[1]};
	}
	#endregion


	#region Private Methods
	private void Initialize ()
	{
		int arrayIndex;
		string prePatternString = null;
		var prePatternsRef = PrePatternsModel.PrePatterns;

		for (int i = 0; i < nStrings; ++i)
		{
			for (int j = 0; j < mColumns; ++j)
			{
				arrayIndex = i * mColumns + j;
				emptyCells.Add (new CellStructure (arrayIndex, i, j));
				tokens[arrayIndex] = 0;
				//				cellWeights[arrayIndex] = 0f;
			}
		}

		// patterns init
		arrayIndex = 0; // re-use
		foreach (var pair in prePatternsRef)
		{
			//			Debug.Log (pair.Key + " : " + pair.Value);
			prePatternString = GetPatternsString (pair.Key, "x");
			//			Debug.Log (prePatternString);
			patternWeights[arrayIndex] = pair.Value;
			patternAttack[arrayIndex] = new Regex (prePatternString.Replace ("x", "2"));
			patternDefence[arrayIndex] = new Regex (prePatternString.Replace ("x", "1"));

//			Debug.Log (prePatternString + " : " + patternAttack[arrayIndex]);

			++arrayIndex;
		}
		// DEBUGS // //
		//		foreach (Regex strRX in patternAttack)
		//		{
		//			Debug.Log (strRX);
		//		}
		//		foreach (CellStructure cell in freeCells)
		//		{
		//			Debug.Log (cell.CellID);
		//		}
		//		AITurn ();
		//		AITurn (0, 0);
		//		AITurn (0, 1);
		//		AITurn (0, 2);
		//		AITurn (0, 3);
		// // // // // // // 
	}

	// listOfCS for player or AI
	private void PutAToken (int strIndex, int colIndex, List <CellStructure> listOfCS, bool isPlayer, bool fixIndexes = false)
	{
//		if (fixIndexes)
//		{
//			strIndex = FixIndex (strIndex, nStrings);
//			colIndex = FixIndex (colIndex, mColumns);
//		}

		int id = strIndex * mColumns + colIndex;
		int csIndex = GetCellIndexByID (id, emptyCells);

		if (csIndex < 0)
		{
//			Debug.Log ("AI: Cell isn't empty");
		}
		else
		{
			CellStructure tmpCS = emptyCells[csIndex];
			emptyCells.RemoveAt (csIndex);
			listOfCS.Add (tmpCS);

//			Debug.Log ("cellID: " + tmpCS.CellID + "; id: " + id);

			if (isPlayer)
			{
				tokens [id] = 1;
			}
			else
			{
				tokens [id] = 2;
			}
			//			cellWeights[id] = -1f;
		}
	}


	private void AIFirstTurn (int playerX, int playerY, bool firstInGame = false)
	{
		if (firstInGame)
		{
			aiReturnXCoord = nStrings / 2;
			aiReturnYCoord = mColumns / 2;
//			PutAToken (aiReturnXCoord, aiReturnYCoord, aiCells, false); 
		}
		else
		{
			PutAToken (playerX, playerY, playerCells, true, !restrictedField);
			int[] randomNeighbourCellCoordinates = GetRandomNeighbourCell (playerCells[0]);
			aiReturnXCoord = randomNeighbourCellCoordinates[0];
			aiReturnYCoord = randomNeighbourCellCoordinates[1];
		}
//		return new int[2] {xCellCoord, yCellCoord};
	}

	// return int[i, j] coordinates of the empty cell where AI should put a token
	private void CalculateAITurn (bool isFieldRestricted = false)
	{		
		bool flag;// = true;
			
//		if (isFieldRestricted)
//		{
		flag = RecalculateCellsWeights ();
//		}
//		else
//		{
////			flag = RecalculateCellsWeights ();
//		}

		if (flag)	// change TotalWeights of empty cells
		{
			float maxWeight = -1;
			var bestCellsIndexes = new List <int[]> ();	// pairs [i][j]

			foreach (CellStructure cell in emptyCells)
			{
				if (playerCells.Contains (cell) || aiCells.Contains (cell))
				{
//					Debug.Log ("########BUG 1##################");
					continue;
				}
				//			Debug.Log ("[" + cell.i + "][" + cell.j + "] = " + cell.TotalCellWeight);
				if (cell.TotalCellWeight >= maxWeight)
				{
					if (cell.TotalCellWeight > maxWeight)
					{
						bestCellsIndexes.Clear ();
					}

					maxWeight = cell.TotalCellWeight;
					bestCellsIndexes.Add (new int[2] {cell.i, cell.j});
					//				Debug.Log ("[" + cell.i + "][" + cell.j + "] = " + cell.TotalCellWeight);
				}
			}

			if (bestCellsIndexes.Count == 1)
			{
				//			return bestCellsIndexes[0];	
				aiReturnXCoord = bestCellsIndexes[0][0];
				aiReturnYCoord = bestCellsIndexes[0][1];
			}
			else if (bestCellsIndexes.Count > 1)
			{
				int randomCoordsIndex = random.Next (0, bestCellsIndexes.Count);	
				aiReturnXCoord = bestCellsIndexes[randomCoordsIndex][0];
				aiReturnYCoord = bestCellsIndexes[randomCoordsIndex][1];
				//			return bestCellsIndexes[randomCoordsIndex];
			}
			else
			{
				aiReturnXCoord = -1;
				aiReturnYCoord = -1;
			}
		}
		else
		{			
//			foreach (List <CellStructure> list in priorityCellsByPatterns)
			foreach (List <int> list in priorityCellIds)
			{				
//				++randIndex;
//				foreach (int cellId in list)
//				{
//					Debug.Log ("---dl: " + cellId);
//				}

				if (list.Count > 1)
				{
					int randIndex = random.Next (0, list.Count);
//					Debug.Log ("GETCELLID : " + list[randIndex]);
					CellStructure cell = GetCellByID (list[randIndex], emptyCells);

					aiReturnXCoord = cell.i;
					aiReturnYCoord = cell.j;
//					aiReturnXCoord = list[randIndex];
//					aiReturnYCoord = list[randIndex];
					Debug.Log ("d 1.0: " + cell.CellID + ": " + aiReturnXCoord + ", " + aiReturnYCoord);
					break;
				}
				else if (list.Count > 0)	// == 1
				{
					if (playerCells.Count > 1)	// bug case
					{
//						Debug.Log ("GETCELLID : " + list[0]);
						CellStructure cell = GetCellByID (list[0], emptyCells);

						aiReturnXCoord = cell.i;
						aiReturnYCoord = cell.j;
					}
					Debug.Log ("d 1.1: " + aiReturnXCoord + ", " + aiReturnYCoord);
//					Debug.Log ("-----" + randIndex + ": " + list);
					break;
				}
			}
			//			if (priorityCellsByPatterns[0].Count > 0)
			//			{
			//				randIndex = random.Next (0, priorityCellsByPatterns[0].Count);
			//				aiReturnXCoord = priorityCellsByPatterns[0][randIndex].i;
			//				aiReturnXCoord = priorityCellsByPatterns[0][randIndex].j;
			//			}
			//			else
			//			{
			//				
			//			}
		}
		//		return new int[] {-1, -1};;
	}


	private bool RecalculateCellsWeights ()
	{
		//		Debug.Log ("#################################################");
		bool returnValue = true;
		int i;
		int j;
		int iNeighbour;
		int jNeighbour;
		string neighbourCells;
		string leapCells;
		string[] cellsPatterns = new string[2];

		// build a string of lines with neighbour 4 cells for each cell in every direction (horizontal, vertical, 2x diagonal);
		// like "002271111", where '7' is current cell, '1' is players tokens, '2' is AI tokens, '0' is empty cells;
		foreach (CellStructure cell in emptyCells)
		{			
			//debug
			if (playerCells.Contains (cell) || aiCells.Contains (cell))
			{
//				Debug.Log ("BUG");
				continue;
			}

			i = cell.i;
			j = cell.j;
			cell.TotalCellWeight = 0f;//cell.AttackWeight + cell.DefenceWeight;
			//			cellWeights[cell.CellID] = cell.AttackWeight + cell.DefenceWeight;
			cell.AttackWeight = 0;
			cell.DefenceWeight = 0;


			for (int direction = 1; direction < 5; ++direction)
			{
				neighbourCells = "";
				leapCells = "";

				for (int offset = -4; offset < 5; ++offset)
				{
					if (offset == 0)
					{
						neighbourCells += "7";
						leapCells += "7";
						continue;
					}

					cellsPatterns = GetPatternState (restrictedField, direction, i, j, offset, neighbourCells, leapCells);
					neighbourCells = cellsPatterns[0];
					leapCells = cellsPatterns[1];
				}
				//					Debug.Log ("[" + i + "][" + j + "]: \"" + neighbourCells + "\"");
				//		open four - last turn ai's victory
				if (patternAttack[0].IsMatch (neighbourCells) || patternAttack[0].IsMatch (leapCells))
				{
//					priorityCellsByPatterns[0].Clear ();	// remove possible def
//					priorityCellsByPatterns[0].Add (cell);
					//	~same~	//
					priorityCellIds[0].Clear ();			// remove possible def
					priorityCellIds[0].Add (cell.CellID);


					Debug.Log ("---000 added: " + cell.i + ", " + cell.j);

					return false;
				}
				// (possibly ai have no open four) player's open four, have to defend
				if (patternDefence[0].IsMatch (neighbourCells) || patternDefence[0].IsMatch (leapCells))
				{
//					priorityCellsByPatterns[0].Add (cell);
					//	~same~	//
					priorityCellIds[0].Add (cell.CellID);
//					Debug.Log ("---001 added: " + cell.i + ", " + cell.j);
					returnValue = false;
				}
//				Debug.Log ("LC: " + leapCells);
				for (int patternIndex = 1; patternIndex < patternWeights.Length; ++patternIndex)
				{
					if (patternAttack[patternIndex].IsMatch (neighbourCells))
					{
						cell.AttackWeight += patternWeights[patternIndex];
//						Debug.Log ("match : " + patternAttack[patternIndex] + "; nc : " + neighbourCells);
					}
					if (patternAttack[patternIndex].IsMatch (leapCells))
					{
						cell.AttackWeight += patternWeights[patternIndex];
//						Debug.Log ("match : " + patternAttack[patternIndex] + "; lc : " + leapCells);
					}
					if (patternDefence[patternIndex].IsMatch (neighbourCells))
					{
						cell.DefenceWeight += patternWeights[patternIndex];
//						Debug.Log ("match : " + patternDefence[patternIndex] + "; nc : " + neighbourCells);
					}
					if (patternDefence[patternIndex].IsMatch (leapCells))
					{
						cell.DefenceWeight += patternWeights[patternIndex];
//						if (direction == 3)
//						{
//							Debug.Log ("match : " + patternDefence[patternIndex] + "; lc : " + leapCells);
//						}
					}
				}

//				cell.TotalCellWeight += 1.1f * cell.AttackWeight + cell.DefenceWeight;	// attack has higher priority
				cell.TotalCellWeight += cell.AttackWeight + cell.DefenceWeight;			// same priority for an attack and defence

				// most important patterns matches || it's not the end of the game - we need calculate other cells weights
				if (CheckAPattern (neighbourCells, cell) || CheckAPattern (leapCells, cell))
				{
					returnValue = false;
				}
			}

//			if (cell.TotalCellWeight > 0)
//			{
//				Debug.Log ("[" + i + "][" + j + "]: " + cell.AttackWeight + ", " + cell.DefenceWeight + ", " + cell.TotalCellWeight);
//			}
		}

		return returnValue;	
	}
	#endregion


	#region Helpfull Methods
	// fix in case of cyclic game field
	private int FixIndex (int index, int rowSize)
	{
		if (index < 0)
		{
			return rowSize + (index % rowSize);
		}
		else if (index >= rowSize)
		{
			return index % rowSize;
		}

		return index;
	}


	private string[] GetPatternState (bool restrictedField, int direction, int i, int j, int offset, string neighbourCells, string leapCells)
	{
		int iNeighbour;
		int jNeighbour;

		if (restrictedField)
		{
			switch (direction)
			{
			case 1:		
				iNeighbour = i + offset;

				if (iNeighbour >= 0 && iNeighbour < nStrings)
				{
					neighbourCells += tokens [iNeighbour * mColumns + j].ToString ();
				}
				// leaping row
				iNeighbour += offset;

				if (iNeighbour >= 0 && iNeighbour < nStrings)
				{
					leapCells += tokens [iNeighbour * mColumns + j].ToString ();
				}
				break;

			case 2:		
				jNeighbour = j + offset;

				if (jNeighbour >= 0 && jNeighbour < mColumns)
				{
					neighbourCells += tokens [i * mColumns + jNeighbour].ToString ();
				}
				// leaping row
				jNeighbour += offset;

				if (jNeighbour >= 0 && jNeighbour < mColumns)
				{
					leapCells += tokens [i * mColumns + jNeighbour].ToString ();
				}
				break;

			case 3:							
				iNeighbour = i + offset;
				jNeighbour = j + offset;

				if (iNeighbour >= 0 && iNeighbour < nStrings && jNeighbour < mColumns && jNeighbour >= 0)
				{
					neighbourCells += tokens [iNeighbour * mColumns + jNeighbour].ToString ();
				}

				iNeighbour += offset;
				jNeighbour += offset;

				if (iNeighbour >= 0 && iNeighbour < nStrings && jNeighbour < mColumns && jNeighbour >= 0)
				{
					leapCells += tokens [iNeighbour * mColumns + jNeighbour].ToString ();
				}
				break;

			case 4:							
				iNeighbour = i - offset;
				jNeighbour = j + offset;

				if (iNeighbour >= 0 && iNeighbour < nStrings && jNeighbour < mColumns && jNeighbour >= 0)
				{
					neighbourCells += tokens [iNeighbour * mColumns + jNeighbour].ToString ();
				}

				iNeighbour -= offset;
				jNeighbour += offset;

				if (iNeighbour >= 0 && iNeighbour < nStrings && jNeighbour < mColumns && jNeighbour >= 0)
				{
					leapCells += tokens [iNeighbour * mColumns + jNeighbour].ToString ();
				}
				break;
			}
		}
		else
		{
			switch (direction)
			{
			case 1:			// vertical			
				iNeighbour = FixIndex (i + offset, nStrings);
				neighbourCells += tokens [iNeighbour * mColumns + j].ToString ();
				// leaping row
				iNeighbour = FixIndex (iNeighbour + offset, nStrings);
				leapCells += tokens [iNeighbour * mColumns + j].ToString ();
				break;

			case 2:		// horizontal
				jNeighbour = FixIndex (j + offset, mColumns);
				neighbourCells += tokens [i * mColumns + jNeighbour].ToString ();
				// leaping row
				jNeighbour = FixIndex (jNeighbour + offset, mColumns);
				leapCells += tokens [i * mColumns + jNeighbour].ToString ();
				break;

			case 3:		// diagonal 1		
				iNeighbour = FixIndex (i + offset, nStrings);
				jNeighbour = FixIndex (j + offset, mColumns);
				neighbourCells += tokens [iNeighbour * mColumns + jNeighbour].ToString ();
				// leaping row
				iNeighbour = FixIndex (iNeighbour + offset, nStrings);
				jNeighbour = FixIndex (jNeighbour + offset, mColumns);
				leapCells += tokens [iNeighbour * mColumns + jNeighbour].ToString ();
				break;

			case 4:		// diagonal 2		
				iNeighbour = FixIndex (i + offset, nStrings);
				jNeighbour = FixIndex (j - offset, mColumns);
				neighbourCells += tokens [iNeighbour * mColumns + jNeighbour].ToString ();

//				iNeighbour += offset;
//				jNeighbour += offset;
				// leaping row
				iNeighbour = FixIndex (iNeighbour + offset, nStrings);
				jNeighbour = FixIndex (jNeighbour - offset, mColumns);
				leapCells += tokens [iNeighbour * mColumns + jNeighbour].ToString ();
				break;
			}
		}

		return new string[2] {neighbourCells, leapCells};
	}


	// get object index in the list
	private int GetCellIndexByID (int id, List <CellStructure> cellsList)
	{
		for (int csIndex = 0; csIndex < cellsList.Count; ++csIndex)
		{
			if (id == cellsList[csIndex].CellID)
			{				
				return csIndex;
			}
		}

		return -1;	// not found
	}


	private CellStructure GetCellByID (int id, List <CellStructure> cells)
	{
		foreach (CellStructure cell in cells)
		{
			if (id == cell.CellID)
			{
				return cell;
			}
		}

		return null;
	}


	//get a string for patterns
	private string GetPatternsString (string pattern, string targetChar)
	{
		int newPatternIndex = 0;
		int targetCharPosition;
		int nextStartPosition = 0;
		string result = "";
		//		int targetCharCount = pattern.Split(targetChar).Length - 1;
		//		string[] arrayOfPatterns = new string[targetCharCount];

		targetCharPosition = pattern.IndexOf (targetChar, nextStartPosition);

		while (targetCharPosition >= 0)
		{
			//			arrayOfPatterns[newPatternIndex] = 
			result += pattern.Substring (0, targetCharPosition) + "7" + pattern.Substring (targetCharPosition + 1) + "|";

			++newPatternIndex;
			targetCharPosition = pattern.IndexOf (targetChar, targetCharPosition + 1);
		}

		return result.Remove(result.Length - 1);
		//		return string.Join ("|", arrayOfPatterns);
	}


	private void CleanUp ()
	{
		foreach (CellStructure cell in emptyCells)
		{
			if (playerCells.Contains (cell) || aiCells.Contains (cell))
			{
//				Debug.Log ("###### REMOVED ########");
				emptyCells.Remove (cell);
			}
		}	
//		foreach (List <CellStructure> list in priorityCellsByPatterns)
//		{
//			list.Clear ();
//		}
		//	~same~	//
		foreach (List <int> list in priorityCellIds)
		{
			list.Clear ();
		}
	}

	// add a cell with match to priorityCellsByPatterns
	private bool CheckAPattern (string neighbourCells, CellStructure cell)
	{
		int priorityIndex = -1;

//		if (patternDefence[0].IsMatch (neighbourCells))
//		{
//			priorityIndex = 0;
//		}
//		else 
		if (patternAttack[1].IsMatch (neighbourCells))
		{
			priorityIndex = 2;
		}
		else if (patternDefence[1].IsMatch (neighbourCells))
		{
			priorityIndex = 1;
		}
		else if (patternAttack[2].IsMatch (neighbourCells) || patternAttack[3].IsMatch (neighbourCells))
		{
			priorityIndex = 4;
		}
		else if (patternDefence[2].IsMatch (neighbourCells) || patternDefence[3].IsMatch (neighbourCells))
		{
			priorityIndex = 3;
		}

		if (priorityIndex > -1)
		{
//			priorityCellsByPatterns[priorityIndex].Add (cell);
			//	~same~	//
			priorityCellIds[priorityIndex].Add (cell.CellID);

			Debug.Log ("---002 added: " + cell.CellID + ": " + cell.i + ", " + cell.j);
			return true;
		}

		return false;
	}


	private int[] GetRandomNeighbourCell (CellStructure inputCell)
	{
		int counter = 0;	// antiLoop
		int i = random.Next (-1, 2) + inputCell.i;
		int j = random.Next (-1, 2) + inputCell.j;
		int id = i * mColumns + j;
		int maxID = nStrings * mColumns - 1;	// last cell id

		while (id < 0 || id > maxID || id == inputCell.CellID)
		{
			Debug.Log (";;;;;; " + i + ", " + j);

			if (counter > 5)	// TODO return something useful
			{
				return new int[2] {-1, -1};
			}
//			Debug.Log (id + " : " + i + ", " + j);
			if (i == 0 && j == 0)
			{
				i = random.Next (-1, 2) + inputCell.i;
				j = random.Next (-1, 2) + inputCell.j;
				id = i * mColumns + j;
				continue;
			}

			if (i < 0)
			{
				i = 1;
			}

			if (i >= nStrings)
			{
				i = inputCell.i - 1;
			}

			if (j < 0)
			{
				j = 1;
			}

			if (j >= mColumns)
			{
				j = inputCell.j - 1;
			}

			id = i * mColumns + j;
			++counter;
		}

//		foreach (CellStructure cell in emptyCells)
//		{
//			if (id == cell.CellID)
//			{
//				return cell;
//			}
//		}

		return new int[2] {i, j};
	}
	#endregion
}
