﻿using UnityEngine;
using System.Collections;

public class CellStructure 
{
	#region Public References
	public int CellID;	// i * numberOfColumns + j
	public int i;	// string index
	public int j;	// column index
//	public int Owner = 0; // 0 - free cell, 1 - player token, 2 - AI token
	public int AttackWeight = 0;
	public int DefenceWeight = 0;
	public float TotalCellWeight = -1f;	// store (atack + def) 
	#endregion


	#region Constructors
	public CellStructure (int id, int strIndex, int colIndex)
	{
		CellID = id;
		i = strIndex;
		j = colIndex;
	}
	#endregion
}
