﻿using System.Collections;
using System.Collections.Generic;


public static class PrePatternsModel 
{
	public static readonly Dictionary<string, int> PrePatterns = new Dictionary<string, int> 
	{
		{"xxxxx", 9999},
		{"0xxxx0", 7000},
		{"0xxxx", 4000},
		{"xxxx0", 4000},
		{"0xxx0", 3000},

		{"xxx0x", 2000},
		{"x0xxx", 2000},
		{"0xxx0x", 2000},
		{"x0xxx0", 2000},
		{"0x0xxx", 2000},
		{"xxx0x0", 2000},
		{"xx0xx", 2000},
		{"0xx0xx", 2000},
		{"xx0xx0", 2000},
		{"00xxx", 1500},
		{"xxx00", 1500},
		{"0xx0x", 800},
		{"x0xx0", 800},
		{"0x0xx", 800},
		{"xx0x0", 800},
		{"00xx0", 200},
		{"0xx00", 200},
		{"xx000", 200},
		{"000xx", 200},
		{"0x0x0", 200},
		{"x0x00", 200},
		{"00x0x", 200},
		{"x00x0", 100},
		{"0x00x", 100},
		{"x000x", 80},
//		{"x0000", 50},
//		{"0x000", 50},
//		{"00x00", 50},
//		{"000x0", 50},
//		{"0000x", 50},
	};
}
