﻿using UnityEngine;
using System.Collections;
using System.Threading;


public class AIController
{
//	public static int debugX;
//	public static int debugY;
//	public static int aiX;
//	public static int aiY;

	private bool notDone = true;
	private int aiIDCount;		// which AI should make its turn now
	private int playersCount;
	private int[] aiResults;
	private AIWorker aiWorker;
	private AIMultiple[] aiWorkers;

	private static System.TimeSpan timeOut = new System.TimeSpan (0, 0, 1);
	/// <summary>
	/// Initialize the specified numberOfColumns and numberOfStrings.
	/// </summary>
	/// <param name="numberOfColumns"> Number of columns. </param>
	/// <param name="numberOfStrings"> Number of strings. </param>
	public AIController (int numberOfColumns, int numberOfStrings, int numberOfAI = -1, int numberOfPlayers = 1)
	{
//		Debug.Log (numberOfStrings + " X " + (numberOfColumns + 1).ToString());
		playersCount = numberOfPlayers + numberOfAI;
		aiResults = new int[2];

		if (numberOfAI < 0 && numberOfPlayers < 0)
		{
			aiWorker = new AIWorker (numberOfStrings, numberOfColumns);	// misplaced arguments
		}
		else if (numberOfAI > 1 && numberOfPlayers == 1)
		{
			aiWorkers = new AIMultiple[numberOfAI];

			for (int aiIndex = 0; aiIndex < numberOfAI; ++aiIndex)	// 0 - for empty cell, 1 - for a player, 2+ for AIs
			{
				aiWorkers[aiIndex] = new AIMultiple (numberOfStrings, numberOfColumns, playersCount, aiIndex + 2); // ai ID starts from '2'
			}
//			aiWorkers[0].DebugMultipleAI ();
		}

//		Debug.Log (FixIndex (3, 3));
//		Debug.Log (FixIndex (4, 3));
//		Debug.Log (FixIndex (5, 3));
//		Debug.Log (FixIndex (-1, 4));
//		Debug.Log (FixIndex (-2, 4));
	}

//	private int FixIndex (int index, int rowSize)
//	{
//		if (index < 0)
//		{
//			return rowSize + (index % rowSize);
//		}
//		else if (index >= rowSize)
//		{
//			return index % rowSize;
//		}
//
//		return index;
//	}

	/// <summary>
	/// Pass no arguments if AI turn is first in the game.
	/// Return [x, y] coordinates of the cell for AI turn. (Error If x or(and) y equal to -1)
	/// </summary>
	/// <param name="playerXCellCoord"> Number of string where player token has been put. </param>
	/// <param name="playerYCellCoord"> Number of column where player token has been put. </param>
//	private void AITurn (int playerXCellCoord = -1, int playerYCellCoord = -1)
	public int[] AITurn (int playerXCellCoord = -1, int playerYCellCoord = -1)
	{			
		var args = new int[2] {playerXCellCoord, playerYCellCoord};
		Thread aiTurn = new Thread (aiWorker.AITurn);

		try
		{
			aiTurn.Start (args);
			aiTurn.Join (timeOut);
		}
		catch (System.Exception ex)
		{
			Debug.Log (ex.Message);
			return new int[2] {-1, -1};
		}

//		aiX = aiWorker.aiReturnXCoord;
//		aiY = aiWorker.aiReturnYCoord;
//		debugX = aiWorker.aiReturnXCoord;
//		debugY = aiWorker.aiReturnYCoord;
//		Debug.Log ("1: " + aiWorker.aiReturnXCoord + ", " + aiWorker.aiReturnYCoord);
//		notDone = false;
		return new int[] {aiWorker.aiReturnXCoord, aiWorker.aiReturnYCoord};
	}

	/// <summary>
	/// Pass no arguments if AI turn after another AI (not after player).
	/// Return [x, y] coordinates of the cell for AI turn. (Error If x or(and) y equal to -1)
	/// </summary>
	/// <param name="playerXCellCoord"> Number of string where player token has been put. </param>
	/// <param name="playerYCellCoord"> Number of column where player token has been put. </param>
// a method for multiple AI 
	public int[] NextAITurn (int playerXCellCoord = -1, int playerYCellCoord = -1)
	{
		var args = new int[2] {playerXCellCoord, playerYCellCoord};

		if (playerXCellCoord < 0)	// ai after ai
		{
			args = null;
		}
		else 		// ai after player
		{
			aiIDCount = -1;
		}

		++aiIDCount;
		Thread aiTurn = new Thread (aiWorkers[aiIDCount].AITurn);

		try
		{
			aiTurn.Start (args);
			aiTurn.Join (timeOut);
		}
		catch (System.Exception ex)
		{
			Debug.Log (ex.Message);
			return new int[2] {-1, -1};
		}

		if (aiWorkers[aiIDCount].aiReturnXCoord < 0)
		{
			foreach (AIMultiple ai in aiWorkers)
			{
				ai.DebugMultipleAI ();
			}
		}

		return new int[] {aiWorkers[aiIDCount].aiReturnXCoord, aiWorkers[aiIDCount].aiReturnYCoord};
	}

//	public IEnumerator WaitForAI (int playerXCellCoord, int playerYCellCoord)
//	{
//		notDone = true;
//		AITurn (playerXCellCoord, playerYCellCoord);
//
//		while (notDone)
//		{
//			yield return null;
//		}
//	}
}
