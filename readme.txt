AI API

1. Create new ai object with a game field parameters ( var aiScript = new AIController (numberOfStrings, numberOfColumns); ).

2. If you want to get new coordinates (x, y) of a cell on the game field where AI token should be placed: 
	
	2.1 use this method from AIController.cs and pass cell coordinates with last turn of a player: 
										- public int[] AITurn (int playerLastTurnX, int playerLastTurnY)

		------- example ----------------------------------------------
		int aiX, aiY;
		int[] aiCellCoordinates;
		aiCellCoordinates = aiScript.AITurn (playerLastTurnX, playerLastTurnY);
		aiX = aiCellCoordinates [0];
		aiY = aiCellCoordinates [1];
		--------------------------------------------------------------

	2.2 If AI should make 1st turn in the game, pass no parameters to AITurn () method)

		------- example ----------------------------------------------
		int aiX, aiY;
		int[] aiCellCoordinates;
		aiCellCoordinates = aiScript.AITurn ();
		aiX = aiCellCoordinates [0];
		aiY = aiCellCoordinates [1];
		--------------------------------------------------------------
